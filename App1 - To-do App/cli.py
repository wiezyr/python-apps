"""To Do App"""
from functions import get_todos, write_todos
import time

now = time.strftime("%b %d, %Y %H:%M:%S")
print("It is ", now)

while True:
    user_action = (input("Type add, show, edit, complete or exit: ")).strip()

    if user_action.startswith("add"):
        todo = user_action[4:]

        todos = get_todos()

        todos.append(todo + "\n")

        write_todos(todos)
        # with open("todos.txt", "w", encoding="UTF-8") as file:
        #     file.writelines(todos)

    elif user_action.startswith("show"):
        todos = get_todos()

        for index, item in enumerate(todos):
            item = item.strip("\n")
            print(index + 1, "-", item)

    elif user_action.startswith("edit"):
        # number = int(input("Number of todo to edit: ")) - 1
        try:
            number = int(user_action[5:]) - 1
            print(number)
            todos = get_todos()

            new_todo = input("Enter new todo:")
            todos[number] = new_todo + "\n"

            write_todos(todos)
        except ValueError:
            print("Invalid command!")
            continue

    elif user_action.startswith("complete"):
        # number = int(input("Number of todo to complete ")) - 1
        try:
            number = int(user_action[9:]) - 1
            todos = get_todos()
            todo_to_complete = todos[number].strip("\n")
            todos.pop(number)
            write_todos(todos)

            message = f"Todo {todo_to_complete} was completed and removed from list."
            print(message)
        except IndexError:
            print("There is no item with that number.")
            continue

    elif user_action.startswith("exit"):
        break
    else:
        print("Unknown command, are you dumb")

print("Bye!")
