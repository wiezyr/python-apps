import requests
from send_email import send_email


URL = "https://newsapi.org/v2/everything?"
parameters = {
    "apiKey": "ccc7f3d972254192ad27406b984ca11d",
    "language": "en",
    "q": "War",
    "sort": "publishedAt",
}


request = requests.get(URL, params=parameters)

content = request.json()
message = f"Subject:  News about {parameters['q']}\n\n"
for article in content["articles"]:
    if article["title"] is not None:
        message += f"{article['title']}\n{article['description']}\n{article['url']}\n\n"


message = message.encode("UTF-8")

send_email(message)
