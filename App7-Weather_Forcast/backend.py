import requests

API_KEY = "a0e81ea8b42279f60dbeb9fe6192288c"
OPN_ENDPOINT = "https://api.openweathermap.org/data/2.5/forecast?"
# GEO_ENDPOINT = "http://api.openweathermap.org/geo/1.0/direct?"


def get_data(place, forcast_days=None):
    params = {"appid": API_KEY, "q": place, "units": "metric"}
    # location = requests.get(url=GEO_ENDPOINT, params=params)

    # loc = location.json()
    # params["lon"] = loc[0]["lon"]
    # params["lat"] = loc[0]["lat"]
    # del params["q"]
    response = requests.get(url=OPN_ENDPOINT, params=params)
    data = response.json()
    filtered_data = data["list"][: 8 * forcast_days]
    
    return filtered_data


# get_data("Wrocław", 5, "Sky")

if __name__ == "__main__":
    print(get_data("Wrocław", 1))
