import streamlit as st
import plotly.express as px
from backend import get_data

st.title("Weather Forecast for Next Days")
place = st.text_input("Place:")

days = st.slider(
    "Forecast Days",
    min_value=1,
    max_value=5,
    help="Select the number of forcasted days",
)

option = st.selectbox("Select data to view", ("Temperature", "Sky"))

st.subheader(f"{option} for the next {days} days in {place}")


if place:
    try:
        data = get_data(place, days)

        if option == "Temperature":
            temperatures = [dane["main"]["temp"] for dane in data]
            dates = [dane["dt_txt"] for dane in data]
            figure = px.line(
                x=dates, y=temperatures, labels={"x": "Date", "y": "Temperature (C)"}
            )
            st.plotly_chart(figure)

        if option == "Sky":
            sky_conditions = [dane["weather"][0]["main"] for dane in data]
            images = {
                "Clouds": "images/cloud.png",
                "Clear": "images/clear.png",
                "Rain": "images/rain.png",
                "Snow": "images/snow.png",
            }
            sky_images = [images[name] for name in sky_conditions]
            st.image(sky_images, width=115)
    except KeyError:
        st.write("That place does not exists")
