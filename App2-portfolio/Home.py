import streamlit as st
import pandas


st.set_page_config(layout="wide")

col1, col2 = st.columns(2)

with col1:
    # st.image("/media/magazyn/Nauka/PythonMegaCourse/App2-portfolio/images/photo.png")
    st.image("images/rys.jpg")
with col2:
    st.title("Wiesiek Rybak")
    content = """
Lorem ipsum dolor sit amet, consectetur adipiscing elit. In et tortor id diam
lacinia dignissim porttitor ac dui. Fusce tincidunt felis vel odio tempor rhoncus.
Phasellus in vestibulum dolor. Curabitur eu quam neque. Donec facilisis,
libero non feugiat faucibus, magna felis luctus nunc, eu sodales ex lectus at libero.
Ut elit nulla, placerat eu nulla quis, suscipit laoreet diam. Nulla lectus ipsum,
suscipit pharetra massa in, consectetur pharetra mi. Integer bibendum pulvinar molestie.
Aenean non ante tristique, facilisis leo non, consequat dolor.
Maecenas dictum pellentesque ex in ultrices.

    """
    st.info(content)

st.write(
    "BElow you can find some of the apps i have built in Python."
    "Feel free to contact me!",
)

col3, empty_col, col4 = st.columns([1.5, 0.5, 1.5])

df = pandas.read_csv("data.csv", sep=";")
with col3:
    for index, row in df[:10].iterrows():
        st.header(row["title"])
        st.write(row["description"])
        st.image("images/" + row["image"])
        st.write(f"[Source Code]({row['url']})")

with col4:
    for index, row in df[10:].iterrows():
        st.header(row["title"])
        st.write(row["description"])
        st.image("images/" + row["image"])
        st.write(f"[Source Code]({row['url']})")
